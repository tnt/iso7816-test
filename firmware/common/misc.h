/*
 * misc.h
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

void reboot(int n);
void wait(int i);

void ncn8025_init(void);
bool ncn8025_get_int_n(void);
void ncn8025_power_up(void);
void ncn8025_power_down(void);
void ncn8025_set_ll(bool pwr, bool rst, bool clk);
bool ncn8025_set_vsel(int mv);
bool ncn8025_set_clk_div(int div);
