/*
 * no2iso7816.h
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <stdint.h>

void iso7816_setup_brg(int F, int D, int div);
void iso7816_init(int div);
void iso7816_put_char(uint8_t c);
int  iso7816_get_char(void);
void iso7816_debug(void);
