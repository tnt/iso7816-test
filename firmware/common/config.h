/*
 * config.h
 *
 * Copyright (C) 2021-2022 Sylvain Munaut
 * SPDX-License-Identifier: MIT
 */

#pragma once

#define MISC_BASE	0x80000000
#define UART_BASE	0x81000000
#define SPI_FLASH_BASE	0x82000000
#define LED_BASE	0x83000000
#define USB_CORE_BASE	0x84000000
#define USB_DATA_BASE	0x85000000
#define ISO7816_BASE	0x86000000
