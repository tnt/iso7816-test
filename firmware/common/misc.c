/*
 * misc.c
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: MIT
 */

#include <stdbool.h>
#include <stdint.h>

#include "config.h"


struct wb_misc {
	uint32_t boot;
	uint32_t rsvd[3];
	struct {
		uint32_t oe;
		uint32_t o;
		uint32_t i;
		uint32_t rsvd;
	} gpio;
} __attribute__((packed,aligned(4)));

static volatile struct wb_misc * const misc_regs = (void*)(MISC_BASE);

#define GPIO_SC_CLK_ENA		(1 << 7)
#define GPIO_SC_INT_N		(1 << 6)
#define GPIO_SC_RST		(1 << 5)
#define GPIO_SC_CMDVCC_N	(1 << 4)
#define GPIO_SC_VSEL_MSK	(3 << 2)
#define GPIO_SC_VSEL_5V		(1 << 2)
#define GPIO_SC_VSEL_3V		(0 << 2)
#define GPIO_SC_VSEL_1V8	(3 << 2)
#define GPIO_SC_CLKDIV_MSK	(3 << 0)
#define GPIO_SC_CLKDIV_1	(2 << 0)
#define GPIO_SC_CLKDIV_2	(3 << 0)
#define GPIO_SC_CLKDIV_4	(1 << 0)
#define GPIO_SC_CLKDIV_8	(0 << 0)


/* Warm reboot */
void
reboot(int n)
{
	misc_regs->boot = (1 << 2) | (n << 0);
}

/* "Calibrated" wait loop */
void
wait(int i)
{
	while (i--)
	{
		for (int j=0; j<132; j++)
			asm("nop");
	}
}


/* NCN8025 init */
void
ncn8025_init(void)
{
	misc_regs->gpio.o = (
		GPIO_SC_CLK_ENA |
		GPIO_SC_RST |
		GPIO_SC_CMDVCC_N |
		GPIO_SC_VSEL_3V |
		GPIO_SC_CLKDIV_4 |
		0
	);

	misc_regs->gpio.oe = (
		GPIO_SC_CLK_ENA |
		GPIO_SC_RST |
		GPIO_SC_CMDVCC_N |
		GPIO_SC_VSEL_MSK |
		GPIO_SC_CLKDIV_MSK |
		0
	);
}

/* NCN8025 int_n line state */
bool
ncn8025_get_int_n(void)
{
	return !!(misc_regs->gpio.i & GPIO_SC_INT_N);
}

/* NCN8025 power up sequence */
void
ncn8025_power_up(void)
{
	/* initial state */
	misc_regs->gpio.o =
		(misc_regs->gpio.o & (GPIO_SC_VSEL_MSK | GPIO_SC_CLKDIV_MSK)) |
		GPIO_SC_CLK_ENA |
		GPIO_SC_CMDVCC_N |
		0;

	/* pause */
	wait(10);	// 1000 us

	/* power up */
	misc_regs->gpio.o &= ~GPIO_SC_CMDVCC_N;

	/* pause */
	wait(1);	// 100 us

	/* disable reset */
	misc_regs->gpio.o |= GPIO_SC_RST;
}

/* NCN8025 power down sequence */
void
ncn8025_power_down(void)
{
	misc_regs->gpio.o = (misc_regs->gpio.o & ~GPIO_SC_RST) | GPIO_SC_CMDVCC_N;
}

/* NCN8025 low level control */
void
ncn8025_set_ll(bool pwr, bool rst, bool clk)
{
	misc_regs->gpio.o = (misc_regs->gpio.o & ~(GPIO_SC_CMDVCC_N | GPIO_SC_RST | GPIO_SC_CLK_ENA)) |
		(pwr ? 0 : GPIO_SC_CMDVCC_N) |
		(rst ? 0 : GPIO_SC_RST) |
		(clk ? GPIO_SC_CLK_ENA : 0);
}

/* NCN8025 select voltage */
bool
ncn8025_set_vsel(int mv)
{
	uint32_t v;

	switch (mv)
	{
	case 5000: v = GPIO_SC_VSEL_5V;  break;
	case 3000: v = GPIO_SC_VSEL_3V;  break;
	case 1800: v = GPIO_SC_VSEL_1V8; break;
	default:
		return false;
	}

	misc_regs->gpio.o = (misc_regs->gpio.o & ~GPIO_SC_VSEL_MSK) | v;

	return true;
}

/* NCN8025 select clock divider */
bool
ncn8025_set_clk_div(int div)
{
	uint32_t v;

	switch (div)
	{
	case 1: v = GPIO_SC_CLKDIV_1; break;
	case 2: v = GPIO_SC_CLKDIV_2; break;
	case 4: v = GPIO_SC_CLKDIV_4; break;
	case 8: v = GPIO_SC_CLKDIV_8; break;
	default:
		return false;
	}

	misc_regs->gpio.o = (misc_regs->gpio.o & ~GPIO_SC_CLKDIV_MSK) | v;

	return true;
}
