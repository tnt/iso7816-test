/*
 * no2iso7816.c
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <stdint.h>

#include "config.h"


struct wb_no2iso7816 {
	uint32_t csr;
	uint32_t _rsvd1;
	uint32_t data;
	uint32_t _rsvd2;
	uint32_t misc;
	uint32_t wt;
	uint32_t brg_rate;
	uint32_t brg_phase;
} __attribute__((packed,aligned(4)));

#define NO2ISO7816_CSR_WT_CE		(1 << 16)
#define NO2ISO7816_CSR_TX_FIFO_EMPTY	(1 << 15)
#define NO2ISO7816_CSR_TX_FIFO_FULL	(1 << 14)
#define NO2ISO7816_CSR_TX_FIFO_OVER	(1 << 13)
#define NO2ISO7816_CSR_TX_FIFO_CLR	(1 << 12)
#define NO2ISO7816_CSR_TX_ERR_PARITY	(1 << 11)
#define NO2ISO7816_CSR_TX_ENA		(1 <<  8)
#define NO2ISO7816_CSR_RX_FIFO_EMPTY	(1 <<  7)
#define NO2ISO7816_CSR_RX_FIFO_FULL	(1 <<  6)
#define NO2ISO7816_CSR_RX_FIFO_OVER	(1 <<  5)
#define NO2ISO7816_CSR_RX_FIFO_CLR	(1 <<  4)
#define NO2ISO7816_CSR_RX_ERR_PARITY	(1 <<  3)
#define NO2ISO7816_CSR_RX_ERR_FRAME	(1 <<  2)
#define NO2ISO7816_CSR_RX_ENA		(1 <<  0)

#define NO2ISO7816_MISC_TX_GT(n)	(((n) & 0xfff) << 20)
#define NO2ISO7816_MISC_TX_RT(n)	((((n)-1) & 0x7) << 16)
#define NO2ISO7816_MISC_TX_EXC_IRQ	(1 << 10)
#define NO2ISO7816_MISC_TX_FIFO_IRQ	(1 <<  9)
#define NO2ISO7816_MISC_TX_NAK_ENA	(1 <<  8)
#define NO2ISO7816_MISC_RX_EXC_IRQ	(1 <<  2)
#define NO2ISO7816_MISC_RX_FIFO_IRQ	(1 <<  1)
#define NO2ISO7816_MISC_RX_NAK_ENA	(1 <<  0)

static volatile struct wb_no2iso7816 * const sc_regs = (void*)(ISO7816_BASE);


void
iso7816_setup_brg(int F, int D, int div)
{
	F *= div;
	sc_regs->brg_rate  = (F << 16) | D;
	sc_regs->brg_phase = (F >> 1) - (D << 2);
}

void
iso7816_init(int div)
{
	/* Core config / reset */
	sc_regs->misc =
		NO2ISO7816_MISC_TX_GT(0) |
		NO2ISO7816_MISC_TX_RT(3) |
		NO2ISO7816_MISC_TX_NAK_ENA |
		NO2ISO7816_MISC_RX_NAK_ENA;

	sc_regs->csr = (
		NO2ISO7816_CSR_TX_FIFO_OVER	|
		NO2ISO7816_CSR_TX_FIFO_CLR	|
		NO2ISO7816_CSR_TX_ERR_PARITY	|
		NO2ISO7816_CSR_TX_ENA		|
		NO2ISO7816_CSR_RX_FIFO_OVER	|
		NO2ISO7816_CSR_RX_FIFO_CLR	|
		NO2ISO7816_CSR_RX_ERR_PARITY	|
		NO2ISO7816_CSR_RX_ERR_FRAME	|
		NO2ISO7816_CSR_RX_ENA		|
		0
	);

	/* Config BRG for default bitrate */
	iso7816_setup_brg(372, 1, div);
}

void
iso7816_put_char(uint8_t c)
{
	sc_regs->data = c;
}

int
iso7816_get_char(void)
{
	uint32_t v = sc_regs->data;
	return (v & (1 << 31)) ? -1 : v;
}

void
iso7816_debug(void)
{
	printf("CSR: %08lx\n", sc_regs->csr);
}
