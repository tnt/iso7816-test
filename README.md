`no2iso7816` Test Project
=========================

This is a quick test project for the [`no2iso7816`](https://github.com/no2fpga/no2iso7816) core targeting the [iCEBreaker](https://github.com/icebreaker-fpga/icebreaker) FPGA board wired up to a [NCN8025 breakout board](https://gitea.osmocom.org/electronics/osmo-small-hardware/src/branch/master/ncn8025-breakout).

See the PCF file for the required wiring.

Currently the firmware is just a base for experimentation. It starts a terminal prompt on the iCEBreaker UART (at 1 Mbaud), and has a few test commands (see `fw_app.c` for the details). The main one is `T` and starts up the card, reads the ATR, does a PPS sequence to change speed and then sends some APDU and reads the response.

```
Command> T
ATR : 3b 9f 96 80 1f c7 80 31 a0 73 be 21 13 67 43 20 07 18 00 00 01 a5
PPS Response : ff 10 96 79
APDU : c0 00 00 7f ff 3f 00 01 00 00 00 00 00 15 b1 04 05 06 00 83 8a 83 8a 00 83 00 83 90 00
```


Credits
-------

Big thanks to [sysmocom GmbH](https://sysmocom.de/) for sponsoring the development.


License
-------

Most of the cores/HDL in here is licensed under one of the CERN OHL 2.0
license. See the `doc/` subdirectory for the full license texts and refer
to each file header to know the license applicable to each file.

Most of the software code here is licensed either under LGPL v3 or MIT.
See the `doc/` subdirectory for the full license texts and refer to each
file header to know the license applicable to each file.

Some files have been imported from other projects with compatible licenses.
Refer to each file header for the proper copyright and license information.

The license of component imported through git submodules can vary from the
above. Check each respective submodule documentation and the file headers.
